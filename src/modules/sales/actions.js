export const addProductToCart = (product) => ({
  type: 'ADD_PRODUCT_TO_CART',
  product
});


export const removeFromCart = (product) => ({
  type: 'REMOVE_ITEM',
  product
});

export const resetCart = () => ({
  type: 'RESET_CART'
});

export const changeSalesNote = (e) => ({
  type: 'CHANGE_SALES_NOTE',
  note: e.target.value
});

export const postOrder = () => ({
  type: 'POST_ORDER'
});

export const parkOrder = () => ({
  type: 'PARK_ORDER'
});

export const searchProducts = e => ({
  type: 'SEARCH_PRODUCTS',
  term: e.target.value
});

export const clearSearch = () => ({
  type: 'SEARCH_PRODUCTS',
  term: ''
});

export const fetchProducts = () => ({
  type: 'FETCH_PRODUCTS_SUCCESS',
  products: [{
    title: 'Apple Fizzer',
    id: 1,
    price: 60
  }, {
    title: 'Almond Crushers',
    id: 2,
    price: 150
  },
  {
    title: 'Blue Lagoon',
    id: 3,
    price: 60
  },
  {
    title: 'Blueberry Treat',
    id: 4,
    price: 150
  },
  {
    title: 'Chicken Steak',
    id: 5,
    price: 200
  },
  {
    title: 'Rice Bowl',
    id: 6,
    price: 100
  },
  {
    title: 'Crispy Fried Chicken',
    id: 7,
    price: 150
  },
  {
    title: 'Mint Lime',
    id: 8,
    price: 50
  }]
});

export const fetchOrders = () => ({
  type: 'FETCH_ORDERS_SUCCESS',
  orders: []
});
